
function searchByTitle(title) {
    const xhr = new XMLHttpRequest();
    xhr.onload = () => {
        const data = JSON.parse(xhr.responseText);
        console.info('Search request loaded. Data:', data);

        handleSearchResponse(data);
    };

    xhr.open('GET', BASE_URL + '&s=' + title);
    xhr.send();

    return false;
}

function handleSearchResponse({ Search: searchResults }) {
    const container = document.getElementById('resultsContainer');
    container.innerText = '';

    for (let result of searchResults) {
        container.append(generateResultCard(result));
    }
}

function handleResultCardClick(imdbID) {
    console.info('click registered', imdbID);
    window.location = 'view.html?id=' + imdbID;
}

function generateResultCard(result) {

    const wrapper = document.createElement('div');
    wrapper.className = 'col-12';


    const card = document.createElement('div');
    card.className = 'card m-1 p-1 result-card';
    card.addEventListener('click', () => handleResultCardClick(result.imdbID));

    // Card body
    const body = document.createElement('div');
    body.className = 'result-card--left';

    //Title
    const title = document.createElement('h5');
    title.className = 'text-truncate';
    title.innerText = result.Title;
    body.append(title);

    card.append(body);

    // const image = document.createElement('img');
    // image.className = ' result-card--right';
    // image.src = result.Poster;
    // image.style.width = '250px';
    // card.append(image);


    wrapper.append(card);
    return wrapper;
}
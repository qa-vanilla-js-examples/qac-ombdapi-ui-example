var urlParams = new URLSearchParams(new URL(window.location.href).search);

function getFilmById(id) {
    const xhr = new XMLHttpRequest();
    xhr.onload = () => {
        const data = JSON.parse(xhr.responseText);
        console.info('Search request loaded. Data:', data);

        handleFilmResponse(data);
    };

    xhr.open('GET', BASE_URL + '&i=' + id);
    xhr.send();

    return false;
}

function handleFilmResponse(film) {
    document.title = film.Title;
    const container = document.getElementById('filmDetails');

    for(let k in film) {
        const container = document.createElement('div');
        container.className = 'someClass';


        const para = document.createElement('p');
        para.innerHTML = k;
        container.append(para);
        
    }
}


getFilmById(urlParams.get('id'));